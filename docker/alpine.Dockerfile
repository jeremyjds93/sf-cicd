ARG BASE_IMG=node BASE_TAG=18.12.1-alpine
FROM $BASE_IMG:$BASE_TAG AS slim

# ARG SFDX=/opt/sfdx
# ENV PATH="$SFDX/bin:$PATH"

RUN apk add --update --no-cache \
      jq=~1 bash=~5 curl=~7 python3=~3 build-base=~0 \
    && npm update -g npm \
    && npm install -g sfdx-cli@latest

# NB: Installing from a tar is faster/leaner but seems to have issues.
# RUN SFDX_URL=https://developer.salesforce.com/media/salesforce-cli/sfdx/channels/stable/sfdx-linux-x64.tar.xz \
#   && mkdir -p "$SFDX" \
#   && curl -sS "$SFDX_URL" | tar xJ -C "$SFDX" --strip-components 1 \
#   && rm -rf "$SFDX/sf" "$SFDX/bin/node" \
#   \
#   && npm update -g npm

COPY scripts/sfdx_helpers.sh /opt

CMD ["bash"]


FROM slim AS full

RUN apk add --update --no-cache openjdk11-jre-headless=~11
