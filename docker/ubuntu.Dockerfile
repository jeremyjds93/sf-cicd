ARG BASE_IMG=node BASE_TAG=18.12.1
FROM $BASE_IMG:$BASE_TAG AS slim

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
      jq bash curl g++ build-essential \
    && rm -rf /var/lib/apt/lists/* \
    && npm update -g npm \
    && npm install -g sfdx-cli@latest

COPY scripts/sfdx_helpers.sh /opt

CMD ["bash"]


FROM slim AS full

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
      openjdk-11-jre-headless \
    && rm -rf /var/lib/apt/lists/*

