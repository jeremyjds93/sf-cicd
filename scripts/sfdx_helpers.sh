#!/bin/bash

# Function to install the Salesforce CLI.
# No arguments.

function install_salesforce_cli() {
  # Salesforce CLI Environment Variables
  # https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_cli_env_variables.htm

  # By default, the CLI periodically checks for and installs updates.
  # Disable (false) this auto-update check to improve performance of CLI commands.
  export SFDX_AUTOUPDATE_DISABLE=false

  # Set to true if you want to use the generic UNIX keychain instead of the Linux libsecret library or macOS keychain.
  # Specify this variable when using the CLI with ssh or "headless" in a CI environment.
  export SFDX_USE_GENERIC_UNIX_KEYCHAIN=true

  # Specifies the time, in seconds, that the CLI waits for the Lightning Experience custom domain to resolve and become available in a newly-created scratch org.
  # If you get errors about My Domain not configured when you try to use a newly-created scratch org, increase this wait time.
  export SFDX_DOMAIN_RETRY=300

  # For package:create, disables automatic updates to the sfdx-project.json file.
  export SFDX_PROJECT_AUTOUPDATE_DISABLE_FOR_PACKAGE_CREATE=true

  # For package:version:create, disables automatic updates to the sfdx-project.json file.
  export SFDX_PROJECT_AUTOUPDATE_DISABLE_FOR_PACKAGE_VERSION_CREATE=true

  # Install Salesforce CLI
  local sfdx_dir=sfdx-cli sfdx_url

  # No need to install again if cached.
  if [[ ! -d $sfdx_dir ]]; then
    mkdir "$sfdx_dir"
    sfdx_url=https://developer.salesforce.com/media/salesforce-cli/sfdx/channels/stable/sfdx-linux-x64.tar.xz
    sfdx_url=${SFDX_URL:-$sfdx_url}
    log_run curl "$sfdx_url" | tar xJ -C "$sfdx_dir" --strip-components 1

    # Need to migrate from sfdx eventually.
    log 'Removing the new unused "sf CLI" for now to save cache space.'
    log_run rm -rf "$sfdx_dir/sf"
  fi

  export PATH="$PWD/$sfdx_dir/bin:$PATH"

  # Another way is to use npm but it's much slower.
  # log_run npm install --global sfdx-cli

  # Output CLI version and plug-in information
  log_run sfdx --version
  log_run sfdx plugins --core
}


# Function to install jq json parsing library.
# No arguments.

function install_jq() {
  local jq_bin=jq
  if [[ ! -x $jq_bin ]]; then
    local vsn
    vsn=$(curl -sS https://github.com/stedolan/jq/releases/latest | grep -oP '(?<=/tag/jq-)[^"]+')
    log_run curl -sSL "https://github.com/stedolan/jq/releases/download/jq-$vsn/jq-linux64" > "$jq_bin"
    chmod +x "$jq_bin"
  fi
  ln -s "$PWD/$jq_bin" /usr/bin/
  log "Installed $($jq_bin --version) to /usr/bin/$jq_bin"
}


function install_jq+sfdx(){ install_jq && install_salesforce_cli; }


# Function to install LWC Jest dependencies.
# Will create or update the package.json with { "test:unit": "sfdx-lwc-jest" } to the scripts property.
# No arguments.

function install_lwc_jest() {
  ensure_package_json_exists

  # Check if the scripts property in package.json contains key for "test:unit"
  local script_value has_lwc_jest
  script_value=$(jq -jr '.scripts."test:unit" | strings' package.json)

  # If no "test:unit" script property, then add one
  if [[ ! $script_value ]]; then
    modify_json_file '.scripts."test:unit" = "sfdx-lwc-jest"' package.json
    log "added test:unit script property to package.json"
  fi

  # Now that we have package.json to store dependency references to
  # and to run our lwc jest test scripts, run npm installer
  log_run npm install
  has_lwc_jest=$(jq -jr '.devDependencies."@salesforce/sfdx-lwc-jest" | strings' package.json)
  [[ $has_lwc_jest ]] || log_run npm install @salesforce/sfdx-lwc-jest --save-dev
}


# Checks if there are LWC Jest Test files in any of the package directories of sfdx-project.json.
# This is necessary because npm will throw error if no test classes are found.
# No arguments.
# Returns `true` or `false`

function check_has_jest_tests() {
  local file_count
  for pkg_dir in $(jq -r '.packageDirectories[]?.path | strings' sfdx-project.json); do
    if [[ -d $pkg_dir ]]; then
      file_count=$(find "$pkg_dir" -type f -path '**/__tests__/*.test.js' -exec printf . \; | wc -c)
      if [[ "$file_count" -gt 0 ]]; then
        printf true
        return
      fi
    fi
  done
  printf false
}


# Runs `npm run test:unit` to execute LWC Jest tests.
# Function takes no arguments.
# Should be called after `setup_lwc`.
# Uses `check_has_jest_tests` to know if there are actually any tests to run.
# If there aren't any jest tests then npm would throw an error and fail the job,
# so we skip running npm if there are no tests, essentially skipping them to avoid error.

function test_lwc_jest() {
  if [[ $(check_has_jest_tests) == true ]]; then
    log_run npm run test:unit
  else
    log 'Skipping lwc tests, found no jest tests in any package directories'
  fi
}


# Function to test the scratch org, such as run Apex tests and/or load data.
# We leverage the script property `test:scratch` in package.json to provide developers a "hook"
# to control exactly how they want their apex test to be executed.
# Arguments:
#     $1 = username or alias of org to test
#     $2 = org name property
# (Assumes you've already authorized to that org)

function test_scratch_org() {
  local org_username="$1"

  if [[ ! $org_username ]]; then
    log "ERROR: No org username provided to 'test_scratch_org' function"
    exit 1
  fi

  ensure_package_json_exists

  # Make directory to output test results
  # https://gitlab.com/help/ci/yaml/README.md#artifactsreports
  local output_dir=tests/apex
  mkdir -p "$output_dir"

  # Check if the scripts property in package.json contains key for "test:scratch"
  local cmd script_value old_org_username status_code

  cmd=(sfdx apex:test:run --code-coverage --result-format junit --wait 10 --output-dir "$output_dir")

  # Use custom "test:scratch" command if available
  script_value=$(jq -jr '.scripts."test:scratch" | strings' package.json)

  if [[ $script_value ]]; then
    export SCRATCH_ORG_USERNAME="$orgname"
    cmd=(npm run test:scratch --silent)
    log "custom 'test:scratch' script: $script_value"
  fi

  # Set the default username so any CLI commands
  # the developer has set in their "test:scratch" script in package.json
  # will operate on the correct environment.
  # Afterwards, restore the original default username, just in case it was different.
  old_org_username=$(log_run sfdx config:get defaultusername --json | jq -jr '.result[0].value')
  sfdx config:set defaultusername="$org_username"
  log_run "${cmd[@]}"
  status_code=$?
  sfdx config:set defaultusername="$old_org_username"

  if [[ $status_code == 0 ]]; then
    [[ $script_value ]] || log "Successfully tested Apex code!"
  else
    log "ERROR: Failed testing Apex code."
    log "status_code=$status_code"
    exit 1
  fi
}


# Function to authenticate to Salesforce.
# Don't expose the auth url to the logs.
# Arguments:
#     $1 = alias to set
#     $2 = Sfdx Auth URL
#     $3 = SFDX AUth URL to use if the previous one isn't set (optional)

function authenticate() {
  local alias_to_set="$1" org_auth_url="$2" file cmd output status_code

  file=$(mktemp)
  printf %s "$org_auth_url" > "$file"
  cmd=(sfdx auth:sfdxurl:store --sfdxurlfile "$file" --setalias "$alias_to_set" --json)
  output=$(log_run "${cmd[@]}")
  status_code=$?
  rm "$file"

  if [[ $status_code == 0 && $(jq -jr '.status?' <<< "$output") == 0 ]]; then
    log 'Authentication successful.'
    log_run sfdx config:set defaultusername="$alias_to_set" \
                            defaultdevhubusername="$alias_to_set"
  else
    log_json "$output"
    log 'ERROR: Failed authentication.'
    log "status_code=$status_code"
    log "status_json=$(jq -jr '.status?' <<< "$output")"
    exit 1
  fi
}


# Function to authenticate for the created scratch org.
# Arguments:
#     $1 = dir with authentication files

function authenticate_scratch() {
  local auth_dir="$1" scratch_org_username scratch_org_auth_url
  scratch_org_username=$(< "$auth_dir/SCRATCH_ORG_USERNAME.txt")
  scratch_org_auth_url=$(< "$auth_dir/SCRATCH_ORG_AUTH_URL.txt")
  authenticate "$scratch_org_username" "$scratch_org_auth_url" 1>&2
  printf %s "$scratch_org_username"
}


# Function to call sfdx org:display and return its JSON output.
# Don't expose the org:display to logs to avoid exposing sensitive information like access tokens.
# Arguments:
#     $1 = target org alias
# Returns a JSON containing the SFDX Auth URL for the given org.
function sfdx_org_display() {
  log_run sfdx org:display --verbose --target-org "$1" --json
}


# Checks a specific limit for the given org
# and exits with error if none remaining.
# Arguments:
#     $1 = target org username whose limits to check
#     $2 = name of the limit to check (e.g. "DailyScratchOrgs" or "Package2VersionCreates")

function assert_within_limits() {
  local org_username="$1" limit_name="$2"
  export limit_name
  log "org_username=$org_username"
  log "limit_name=$limit_name"

  local cmd limit limits
  cmd=(sfdx limits:api:display --target-org "$org_username" --json)
  limits=$(log_run "${cmd[@]}")
  # log_json "$limits"
  limit=$(jq -jr '.result[] | select(.name == env.limit_name)' <<< "$limits")

  # If a limit was found, then check if we are within it
  if [[ -n $limit ]]; then
    local limit_max limit_rem
    limit_max=$(jq -jr '.max' <<< "$limit")
    limit_rem=$(jq -jr '.remaining' <<< "$limit")

    if [[ ! $limit_rem =~ ^[0-9]+$ || $limit_rem = 0 ]]; then
      log "ERROR: Max of $limit_max reached for limit $limit_name"
      [[ $limit_rem != 0 ]] && log "limit_rem=$limit_rem"
      exit 1
    else
      log "$limit_rem of $limit_max remaining for limit $limit_name"
    fi
  else
    log "No limits found for name $limit_name"
  fi
}


# Function to get package info as a JSON.
# Arguments:
#     $1 = dev hub alias
#     $2 = package name (optional, if not set then looks at $PACKAGE_NAME env variable, then in sfdx-project.json for default package directory)
# Returns the package info for the given package name owned by the given dev hub.

function get_package_info() {
  # To make our local variables available to `jq` expressions,
  # we need to export them to the environment. They are still scoped to this function.
  local devhub_username="$1" package_name="$2" default_pkg
  log "devhub_username=$devhub_username"
  log "package_name=$package_name"

  # Check environment variables
  if [[ ! $package_name ]]; then
    log "no package_name argument provided, defaulting to environment variable PACKAGE_NAME"

    # Check for default package directory in sfdx-project.json
    if [[ $PACKAGE_NAME ]]; then
      package_name="$PACKAGE_NAME"
    else
      log "no PACKAGE_NAME environment variable set, defaulting to default package directory in sfdx-project.json"
      default_pkg=$(get_default_package)

      # Check for any package directory in sfdx-project.json
      if [[ $default_pkg ]]; then
        log "using package from packageDirectories:"
        log "$default_pkg"
        package_name=$(jq -jr '.package' <<< "$default_pkg")
      else
        log 'no package name found inside "packageDirectories" in sfdx-project.json'
      fi
    fi
  fi

  # Giving up
  if [[ ! $package_name ]]; then
    log "ERROR: Package name not specified. Set the PACKAGE_NAME environment variable or specify a default package directory in sfdx-project.json."
    exit 1
  fi

  # Retrieve the package id for the package name
  local cmd package_list package_id package_info_query package_info_name package_info
  cmd=(sfdx package:list --target-hub-org "$devhub_username" --json)
  package_list=$(log_run "${cmd[@]}")

  export package_name
  package_id=$(jq -jr '.packageAliases[env.package_name]? | strings' sfdx-project.json)

  if [[ $package_id ]]; then
    log "using package id from .packageAliases[$package_name] in sfdx-project.json:"
    log "package_id=$package_id"
    export package_id
    package_info_query='.result[] | select(.Id == env.package_id)'
  else
    log "using package name to find the package details:"
    log "package_name=$package_name"
    package_info_query='.result[] | select(.Name == env.package_name)'
  fi

  package_info=$(jq -jr "$package_info_query" <<< "$package_list")
  log 'package_info:'
  log_json "$package_info"

  if [[ ! $package_info ]]; then
    log "ERROR: We could not find a package with name '$package_name' owned by this Dev Hub org."
    log 'package_list:'
    log_json "$package_list"
    exit 1
  fi

  package_info_name=$(jq -jr ".Name" <<< "$package_info")

  if [[ $package_name != "$package_info_name" ]]; then
    log 'WARN: The given package name does not match the Package2 name.'
    log "package_name=$package_name"
    log "package_info_name=$package_info_name"
  fi

  # Send back the whole package info JSON
  printf %s "$package_info"
}


# Function to ensure sfdx-project.json has a package alias entry for the package id.
# Arguments:
#     $1 = dev hub alias that owns the package id
#     $2 = package id, the value for the package alias

function add_package_alias() {
  local devhub_username="$1" package_info="$2" package_id package_name
  export package_id package_name

  package_id=$(jq -jr '.Id' <<< "$package_info")
  package_name=$(jq -jr '.Name | strings' <<< "$package_info")

  if [[ ! $package_name ]]; then
    log "ERROR: We could not find a package with id '$package_id' owned by this Dev Hub org."
    exit 1
  fi

  # Check if the alias property in sfdx-project.json contains key for package name
  package_alias=$(jq -jr '.packageAliases[env.package_name]? | strings' sfdx-project.json)

  # If no package alias, then add one
  if [[ ! $package_alias ]]; then
    modify_json_file '.packageAliases[env.package_name]? = env.package_id' sfdx-project.json
    log "added package alias property to sfdx-project.json"
    log_json "$(jq 'with_entries(select(.key == "packageAliases"))' sfdx-project.json)"
  fi
}


# Function to build a package version.
# Arguments:
#     $1 = dev hub alias
#     $2 = package id
# Returns the created subscriber package version id.

function build_package_version() {
  local devhub_username="$1" package_info="$2" package_id package_name version_number
  log "devhub_username=$devhub_username"

  package_id=$(jq -jr '.Id' <<< "$package_info")
  package_name=$(jq -jr '.Name' <<< "$package_info")
  log "package_id=$package_id"
  log "package_name=$package_name"

  if [[ $INCREMENT_RELEASED_VERSION == true ]]; then
    version_number=$(get_incremented_latest_release_version "$devhub_username" "$package_id")
  else
    export package_name
    version_number=$(jq -jr '.packageDirectories[]? | select(.package == env.package_name).versionNumber | strings' sfdx-project.json)
  fi

  if [[ ! $version_number ]]; then
    log "ERROR: failed to determine the package version"
    log "INCREMENT_RELEASED_VERSION=$INCREMENT_RELEASED_VERSION"
    log 'package_info:'
    log_json "$package_info"
    exit 1
  fi

  log "version_number=$version_number"

  create_package_version "$devhub_username" "$package_id" "$version_number"
}

function get_incremented_latest_release_version() {
  local devhub_username="$1" package_id="$2"
  # Calculate next version number.
  # If the latest package version is released then
  # we need to increment the major or minor version numbers.
  local cmd output last_package_version is_released major_version minor_version patch_version
  cmd=(sfdx package:version:list --target-hub-org "$devhub_username" --packages "$package_id" --concise --released --json)
  output=$(log_run "${cmd[@]}")
  log_json "$output"

  last_package_version=$(jq '.result | sort_by(-.MajorVersion, -.MinorVersion, -.PatchVersion, -.BuildNumber)[0]' <<< "$output")

  is_released=$(jq -jr '.IsReleased | booleans // false' <<< "$last_package_version")
  major_version=$(jq -jr '.MajorVersion | numbers // 1' <<< "$last_package_version")
  minor_version=$(jq -jr '.MinorVersion | numbers // 0' <<< "$last_package_version")
  patch_version=$(jq -jr '.PatchVersion | numbers // 0' <<< "$last_package_version")

  [[ $is_released == true ]] && minor_version=$((minor_version + 1))

  printf %s "$major_version.$minor_version.$patch_version.NEXT"
}


# Function to create a package version.
# Arguments:
#     $1 = dev hub alias
#     $2 = package id
#     $3 = version number
# Returns the created subscriber package version id.

function create_package_version() {
  local devhub_username="$1" package_id="$2" version_number="$3"

  local cmd create_json package_request_id status slept timeout report_json subscriber_package_version_id
  cmd=(sfdx package:version:create --package "$package_id" --version-number "$version_number" --installation-key-bypass --code-coverage --target-hub-org "$devhub_username" --json)
  create_json=$(log_run "${cmd[@]}")
  log_json "$create_json"
  package_request_id=$(jq -jr '.result?.Id? | strings' <<< "$create_json")
  status=$(jq -jr '.result.Status' <<< "$create_json")

  if [[ ! $package_request_id ]]; then
    log "ERROR: Did not receive a package request id."
    exit 1;
  fi

  slept=0
  timeout=$((VERSION_STATUS_POLL_TIMEOUT * 60))

  while true; do
    SLEEP="${VERSION_STATUS_POLL_INTERVAL:-45}"
    slept=$((slept + SLEEP))

    if ((timeout && slept > timeout)); then
      log "ERROR: Timed out after $((timeout / 60))m polling the package status report."
      exit 1
    fi

    log "status=$status"
    log_run sleep "$SLEEP"

    cmd=(sfdx package:version:create:report -i "$package_request_id" --json)
    report_json=$(log_run "${cmd[@]}")
    status=$(jq -jr '.result[0].Status' <<< "$report_json")

    case $status in
      Success)
        subscriber_package_version_id=$(jq -jr '.result[0].SubscriberPackageVersionId' <<< "$report_json")
        log "subscriber_package_version_id=$subscriber_package_version_id"
        log_json "$report_json"
        break
        ;;
      Error)
        log "ERROR: Failed creating new package version for package id '$package_id':"
        log_json "$report_json"
        exit 1
    esac
  done

  printf %s "$subscriber_package_version_id"
}


# Install a package version.
# Arguments:
#     $1 = target username where to install package version
#     $2 = package version id to install

function install_package_version() {
  local org_username="$1" package_version_id="$2" cmd output status_json status_text has_errors

  log "org_username=$org_username"
  log "package_version_id=$package_version_id"

  if [[ ! $org_username ]]; then
    log "ERROR: No org username provided to 'install_package_version' function"
    exit 1
  fi

  if [[ ! $package_version_id ]]; then
    log "ERROR: No package version id provided to 'install_package_version' function"
    exit 1
  fi

  # install the package
  cmd=(sfdx package:install --target-org "$org_username" --package "$package_version_id" --wait 10 --publish-wait 10 --no-prompt --json)
  output=$(log_run "${cmd[@]}")
  log_json "$output"

  # assert no error response
  status_json=$(jq -jr '.status?' <<< "$output")
  status_text=$(jq -jr '.result?.Status?' <<< "$output")
  has_errors=$(jq -jr '.result?.Errors? | . != null and . != []' <<< "$output")

  if [[ $status_json == 0 && $status_text == SUCCESS ]]; then
    log "Successfully installed package [$package_version_id]"
  else
    log "status_json=$status_json"
    log "status_text=$status_text"
    log "has_errors=$has_errors"
    if ((status_json > 0)) || [[ $has_errors == true ]]; then
      log "ERROR: Failed installing package [$package_version_id]"
      exit 1
    fi
  fi
}


# Promote package version.
# Only required in production.
# Arguments:
#     $1 = target dev hub that owns the package to promote
#     $2 = package version id to promote

function promote_package_version() {
  local devhub_username="$1" package_version_id="$2" cmd
  log "devhub_username=$devhub_username"
  log "package_version_id=$package_version_id"
  cmd=(sfdx package:version:promote --target-hub-org "$devhub_username" --package "$package_version_id" --no-prompt --json)
  log_json "$(log_run "${cmd[@]}")"
}


# Return the instance URL of an org.
# Arguments:
#     $1 = org alias
function get_instance_url() {
  sfdx org:display --target-org "$1" --json | jq -jr '.result.instanceUrl'
}


# Return the Auth URL of a sandbox.
# Arguments:
#     $1 = branch slug
function get_sandbox_auth_url() {
  local auth_url_var_name="SANDBOX_AUTH_URL_${1//-/_}"
  printf %s "${!auth_url_var_name}"
}


# Populate the URL artifact with HTML to redirect to an environment.
# Generates the URL to open the given org, and writes it to a file ENVIRONMENT.html to be shared as an artifact between stages.
# NOTE: This is a tokenized URL and must be kept secret!
# Arguments:
#     $1 = org alias to get access url to
# (Assumes you are authorized to that org)

function populate_scratch_org_redirect_html() {
  local org_username="$1" cmd url environment_html

  if [[ ! $org_username ]]; then
    log "ERROR: No org username provided to 'populate_scratch_org_redirect_html' function"
    exit 1
  fi

  cmd=(sfdx org:open --target-org "$org_username" --url-only --json)
  url=$(log_run "${cmd[@]}" | jq -jr ".result.url")

  printf %s "$url" > "$SCRATCH_AUTH_DIR/ENVIRONMENT_URL.txt"

  environment_html="<script>window.onload=function(){window.location.href=\"$url\"}</script>"
  printf %s "$environment_html" > "$SCRATCH_AUTH_DIR/ENVIRONMENT.html"
  log 'To browse the scratch org, click "Browse" under "Job artifacts" and select "ENVIRONMENT.html".'
}


# Create a scratch org and deploy to it.
# Arguments:
#     $1 = dev hub alias
#     $2 = org name for the scratch org
# Populates artifacts for the username and the auth url

function deploy_scratch_org() {
  local devhub="$1" orgname="$2" scratch_org_username
  assert_within_limits "$devhub" ActiveScratchOrgs
  assert_within_limits "$devhub" DailyScratchOrgs
  scratch_org_username=$(create_scratch_org "$devhub" "$orgname")
  printf %s "$scratch_org_username" > "$SCRATCH_AUTH_DIR/SCRATCH_ORG_USERNAME.txt"
  sfdx_org_display "$scratch_org_username" > "$SCRATCH_AUTH_DIR/SCRATCH_ORG_DISPLAY.json"
  jq -jr '.result.sfdxAuthUrl' "$SCRATCH_AUTH_DIR/SCRATCH_ORG_DISPLAY.json" > "$SCRATCH_AUTH_DIR/SCRATCH_ORG_AUTH_URL.txt"
  INSTALL_DEPENDENCIES=true install_dependencies "$devhub" "$scratch_org_username"
  push_to_scratch_org "$scratch_org_username"
  populate_scratch_org_redirect_html "$scratch_org_username"
  log "Deployed to scratch org $orgname for $scratch_org_username"
}


# Install defined package dependencies.
# Arguments:
#     $1 = org username

function install_dependencies() {
  local devhub="$1" org_username="${2:-1}" default_pkg package_version_id package_id version cmd packages

  if [[ $INSTALL_DEPENDENCIES != true ]]; then
    log "INSTALL_DEPENDENCIES=$INSTALL_DEPENDENCIES"
    return 0
  fi

  default_pkg=$(get_default_package)

  # Dependencies must be installed in the order they are listed.
  for dependency in $(jq -rc '.dependencies[]? | select(. >= {})' <<< "$default_pkg"); do
    log "dependency=$dependency"
    package_version_id=$(jq -jr '.subscriberPackageVersionId | strings' <<< "$dependency")

    if [[ $package_version_id ]]; then
      log_run install_package_version "$org_username" "$package_version_id"
      continue
    fi

    package_id=$(jq -jr '.packageId | strings' <<< "$dependency")

    if [[ $package_id ]]; then
      version=$(jq -jr '.versionNumber | strings' <<< "$dependency")

      cmd=(sfdx package:version:list --target-hub-org "$devhub" --packages "$package_id" --json)
      packages=$(log_run "${cmd[@]}")

      local filter_expr select_expression package

      if [[ $version ]]; then
        IFS='.' read -ra parts <<< "$version"
        filter_expr=$([[ ${parts[3]} =~ LATEST|^\s*$ ]] || printf '     .BuildNumber == %s' "${parts[3]}")
        filter_expr=$filter_expr$([[ ${parts[2]} == 0 && ! $filter_expr ]] || printf ' and .PatchVersion == %s' "${parts[2]}")
        filter_expr=$filter_expr$([[ ${parts[1]} == 0 && ! $filter_expr ]] || printf ' and .MinorVersion == %s' "${parts[1]}")
        filter_expr=$filter_expr$([[ ${parts[0]} == 0 && ! $filter_expr ]] || printf ' and .MajorVersion == %s' "${parts[0]}")

        if [[ $filter_expr ]]; then
          filter_expr=${filter_expr:5} # Slice off leading ' and '.
          select_expression="| map(select($filter_expr)) "
        fi
      fi

      package=$(jq -jr ".result $select_expression| sort_by(-.MajorVersion, -.MinorVersion, -.PatchVersion, -.BuildNumber)[0]" <<< "$packages")
      log_json "$package"

      package_version_id=$(jq -jr '.SubscriberPackageVersionId | strings' <<< "$package")

      if [[ ! $package_version_id ]]; then
        log "ERROR: couldn't find SubscriberPackageVersionId for package_id '$package_id'"
        log_json "$dependency"
        exit 1
      fi

      log_run install_package_version "$org_username" "$package_version_id"
      continue
    fi

    if [[ ! $(jq -jr '.path | strings' <<< "$dependency") ]]; then
      log "WARN: dependency should have one of these required keys: subscriberPackageVersionId, packageId, path"
      log_json "$dependency"
    fi
  done
}


# Check a scratch org's life status
# Arguments:
#     $1 = directory with credential files

function check_scratch_org() {
  local auth_dir="$1" scratch_org_username scratch_org_auth_url cmd output status

  log_run mkdir -p "$auth_dir" && touch "$auth_dir/.this_dir_contains_auth_files"
  log_run ls -l "$auth_dir"

  if [[ -f "$auth_dir/SCRATCH_ORG_AUTH_URL.txt" ]]; then
    scratch_org_username=$(< "$auth_dir/SCRATCH_ORG_USERNAME.txt")
    scratch_org_auth_url=$(< "$auth_dir/SCRATCH_ORG_AUTH_URL.txt")
    authenticate "$scratch_org_username" "$scratch_org_auth_url"

    cmd=(sfdx org:display -o "$scratch_org_username" --json)
    output=$(log_run "${cmd[@]}")

    status=$(jq -jr '.result.connectedStatus' <<< "$output")

    if [[ $status == 'Connected' ]]; then
      log "Scratch org '$CI_COMMIT_REF_NAME' is still active."
    else
      log "ERROR: Status of scratch org '$CI_COMMIT_REF_NAME' is not 'Connected' but '$status'."
      return 1
    fi
  else
    log "INFO: No scratch org exists for '$CI_COMMIT_REF_NAME'."
    return 1
  fi
}


# Truncate a scratch org's name if it's longer than 80 characters
# Arguments:
#     $1 = org name for the scratch org
# This name is stored in the ScratchOrgInfo SObject

function truncate_scratch_org_name() {
  local orgname="$1"

  if [[ $2 == --warn ]] && ((${#orgname} > 80)); then
    log 'WARN: truncating scratch org name as it is longer than the maximum allowed 80 characters'
    log "orgname=$orgname"
    orgname=${orgname::80}
    log "truncating scratch org name to '$orgname'"
  fi

  printf %s "$orgname"
}


# Create a new scratch org
# Arguments:
#     $1 = dev hub alias
#     $2 = org name for the scratch org
# Populates the artifacts for username and auth url
# Returns the newly-created scratch org username.

function create_scratch_org() {
  local devhub="$1" orgname="$2" lifespan="$3" cmd script_value output status_code scratch_org_username
  lifespan=${lifespan:-${SCRATCH_ORG_LIFESPAN:-7}}
  orgname=$(truncate_scratch_org_name "$orgname" --warn)

  cmd=(sfdx org:create:scratch --target-dev-hub "$devhub" --wait 10 --duration-days "$lifespan" --definition-file config/project-scratch-def.json --json)

  # Use custom "scratch:create" command if available
  script_value=$(jq -jr '.scripts."scratch:create" | strings' package.json)

  if [[ $script_value ]]; then
    export DEVHUB_ORG_NAME="$devhub" SCRATCH_ORG_NAME="$orgname" SCRATCH_ORG_LIFESPAN=$lifespan
    cmd=(npm run scratch:create --silent -- "$orgname")
    log "custom 'scratch:create' script: $script_value"
  else
    export orgname
    modify_json_file '.orgName = env.orgname' config/project-scratch-def.json
  fi

  # Create the scratch org
  output=$(log_run "${cmd[@]}")
  status_code=$?
  log_json "$output"

  scratch_org_username=$(jq -jr '.result?.username? | strings' <<< "$output")

  if [[ ! $scratch_org_username ]]; then
    log 'ERROR: no scratch org user name found.'
    exit 1
  fi

  if [[ $status_code == 0 ]]; then
    [[ $script_value ]] || log "Successfully created scratch org '$orgname'!"
    printf %s "$scratch_org_username"
  else
    log "ERROR: Failed creating scratch org '$orgname'."
    log "status_code=$status_code"
    exit 1
  fi
}


# Get scratch org usernames
# Arguments:
#     $1 = username or alias for the dev hub
#     $2 = org name value for the scratch orgs
# Returns one or more usernames (newline-separated)

function get_scratch_org_usernames() {
  local devhub="$1" orgname="$2" result
  orgname=$(truncate_scratch_org_name "$orgname")
  result=$(sfdx data:query --target-org "$devhub" --query "SELECT SignupUsername FROM ScratchOrgInfo WHERE OrgName='$orgname'" --json)
  printf %s "$(jq -r ".result.records[].SignupUsername" <<< "$result")"
}


# Push to scratch org.
# Arguments
#     $1 = scratch org username

function push_to_scratch_org() {
  local scratch_org_username="$1" cmd script_value old_org_username status_code

  if [[ ! $scratch_org_username ]]; then
    log "ERROR: No scratch org username provided to 'push_to_scratch_org' function"
    exit 1
  fi

  ensure_package_json_exists

  cmd=(sfdx force:source:push)

  # Use custom "scratch:deploy" command if available
  script_value=$(jq -jr '.scripts."scratch:deploy" | strings' package.json)

  if [[ $script_value ]]; then
    export SCRATCH_ORG_USERNAME="$scratch_org_username"
    cmd=(npm run scratch:deploy --silent)
    log "custom 'scratch:deploy' script: $script_value"
  fi

  # Set the default username so any CLI commands
  # the developer has set in their "test:apex" script in package.json
  # will operate on the correct environment.
  # Afterwards, restore the original default username, just in case it was different.
  old_org_username=$(log_run sfdx config:get defaultusername --json | jq -jr '.result[0].value')
  sfdx config:set defaultusername="$scratch_org_username"
  log_run "${cmd[@]}"
  status_code=$?
  sfdx config:set defaultusername="$old_org_username"

  if [[ $status_code == 0 ]]; then
    [[ $script_value ]] || log "Successfully pushed to scratch org '$scratch_org_username'!"
  else
    log "ERROR: Failed pushing to scratch org '$scratch_org_username'."
    log "status_code=$status_code"
    exit 1
  fi
}


# Delete all scratch orgs associated with a ref
# Arguments
#     $1 = dev hub username
#     $2 = org name property of scratch orgs to delete

function delete_scratch_orgs() {
  local devhub_username="$1" scratch_org_name="$2" usernames cmd
  usernames=$(get_scratch_org_usernames "$devhub_username" "$scratch_org_name")
  for scratch_org_username in $usernames; do
    log "Deleting $scratch_org_username"
    cmd=(sfdx data:delete:record --sobject ScratchOrgInfo --target-org "$devhub_username" --where "SignupUsername='$scratch_org_username'" --json)
    log_json "$(log_run "${cmd[@]}")"
  done
}

# Deploys metadata to the org.
# Arguments
#     $1 = org username to deploy to

function deploy_metadata() {
  local org_username="$1" default_pkg source_folder cmd script_value old_org_username status_code

  if [[ ! $org_username ]]; then
    log "ERROR: No org username provided to 'deploy_metadata' function"
    exit 1
  fi

  log_run install_dependencies "$org_username"

  # Check for default package directory in sfdx-project.json
  default_pkg=$(get_default_package)
  source_folder=$(jq -jr '.path' <<< "$default_pkg")

  if [[ $source_folder ]]; then
    log "using package directory: $source_folder"
  else
    # Giving up
    log "ERROR: Default package directory not specified. Specify a default package directory in sfdx-project.json."
    exit 1
  fi

  cmd=(sfdx force:source:deploy --targetusername "$org_username" --sourcepath "$source_folder" --testlevel RunLocalTests --wait 10 --json)

  # Use custom "metadata:deploy" command if available
  script_value=$(jq -jr '.scripts."metadata:deploy" | strings' package.json)

  if [[ $script_value ]]; then
    export SCRATCH_ORG_USERNAME="$scratch_org_username"
    cmd=(npm run metadata:deploy --silent)
    log "custom 'metadata:deploy' script: $script_value"
  fi

  old_org_username=$(log_run sfdx config:get defaultusername --json | jq -jr '.result[0].value')
  sfdx config:set defaultusername="$org_username"
  log_run "${cmd[@]}"
  status_code=$?
  sfdx config:set defaultusername="$old_org_username"

  if [[ $status_code == 0 ]]; then
    [[ $script_value ]] || log "Successfully deployed to $org_username!"
  else
    log "ERROR: Failed deploying to $org_username."
    log "status_code=$status_code"
    exit 1
  fi
}

function get_default_package() {
  local json_file=sfdx-project.json pckg
  pckg=$(jq '.packageDirectories[]? | select(.default == true) | objects' "$json_file")

  if [[ ! $pckg ]]; then
    log "no default package directory found, defaulting to first package directory listed in $json_file"
    # Pick the first directory.
    pckg=$(jq '.packageDirectories[0]? | objects' "$json_file")
  fi

  printf %s "$pckg"
}

function ensure_ci_var() {
  local name="$1"
  if [[ ! ${!name} ]]; then
    log "ERROR: The CI variable $name is not defined!"
    log "Set the variable if it's empty (Settings > CI/CD > Variables)."
    log "If it's protected make sure that the branch is also protected (Settings > Repository > Protected branches)."
    return 1
  fi
}

# Creates a default package.json if the file doesn't exist.
function ensure_package_json_exists() {
  if [[ ! -f package.json ]]; then
    log 'WARN: package.json does not exist! Creating default...'
    log_run npm init -y
  fi
}

# Adds a value defined by a jq query to a json file. Also returns the new json.
function modify_json_file() {
  local query="$1" file="$2" json
  # NB: must assign to a variable first or the pipes will truncate the file.
  json=$(jq "$query" "$file")
  printf '%s\n' "$json" | tee "$file"
}

function print_args() { printf ' %q' "$@" | tail -c+2; }

function log() { echo "$@" >&2; }
function log_run() { log "$ $(print_args "$@")"; "$@"; }
function log_json() { jq . <<< "$1" >&2; }
